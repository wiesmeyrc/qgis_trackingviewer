# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TrackingViewer
                                 A QGIS plugin
 Plugin for showing objects
                              -------------------
        begin                : 2018-02-16
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Christoph Wiesmeyr/AIT Austrian Institute of Technology GmbH
        email                : christoph.wiesmeyr@ait.ac.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import threading as th
import socket
import logging

from PyQt4.Qt import QObject, QThread
from PyQt4.QtCore import pyqtSignal, pyqtSlot

from jsocket_base import JsonClient
from mobile_items import MobileItems


class JSocketProvider(QThread):

    newDataReceived = pyqtSignal(dict)
    registerItem = pyqtSignal(str, str)
    unregisterItem = pyqtSignal(str)
    unregisterAllItems = pyqtSignal()

    def __init__(self, address, port, iface, parent=None):
        """
        This class uses JSockets (from jsocket package) to establish a connection using TCP/IP.
        Received messages are interpreted and mobile_items are created and shown in the qgis window.

        :param address: IP adress to connect to (server has to be set up outside)
        :param port: Port for the connection
        :param iface: QGIS interface object
        :param parent: QT parent
        """

        super(JSocketProvider, self).__init__(parent)
        self.name = 'Standard'
        self.socket = JsonClient(address, port)
        # self.data_receiver_thread = th.Thread(target=self.data_receiving)
        self.iface = iface
        self.mobile_items = MobileItems(self.iface)
        self.mobile_items.subscribePositionProvider(self)

        # logging
        self.log = logging.getLogger('JSocketProvider')
#        self.tracked_objects = {}

    def run(self):

        self.data_receiving()

    def data_receiving(self):

        self.socket.connect()
        self.log.debug('Starting to listen for packages')

        while 1:
            try:
                self.log.debug('Listening for incoming packages')
                msg = self.socket.read_obj()
                self.log.debug('Received package')
                try:
                    self.process_message(msg)
                except:
                    self.log.exception('Unknown error')
            except socket.error:
                self.log.info('Connection broken, exiting')
                break
            except RuntimeError:
                self.log.info('Connection broken, exiting')
                break
            except:
                self.log.exception('Unknown Exception in package parsing')

        self.clean_up()

    def clean_up(self):
        self.socket.close()
        self.unregisterAllItems.emit()

    def process_message(self, msg):

        try:
            mess_type = msg['HEADER']['TYPE']
        except KeyError:
            self.log.warn('Message does not contain the key "HEADER/TYPE"')
            return
        if mess_type == 'register':
            self.register(msg)
        elif mess_type == 'position':
            self.update_position(msg)
        elif mess_type == 'unregister':
            self.unregister(msg)
        else:
            self.log.warn('Unknown message type')

    def register(self, msg):

        try:
            new_uuid = msg['DATA']['TRACKID']
            client_id = msg['DATA'].get('CLIENTID', 'None')
        except KeyError:
            self.log.warn('Could not find required fields in message', exc_info=True)
            return

        self.registerItem.emit(new_uuid, client_id)
        self.log.info('Request registering of Object with ID {0}'.format(new_uuid))

    def update_position(self, msg):

        try:
            uuid = msg['DATA']['properties']['TRACKID']
            time = msg['DATA']['properties']['TIMESTAMP_UNIX']
            point = msg['DATA']['geometry']['coordinates']
            try:
                clientid = msg['DATA']['properties']['CLIENTID']
            except KeyError:
                clientid = None
            lon = point[0]
            lat = point[1]
            data = {'lat': lat, 'lon': lon, 'time': time, 'uuid': uuid, 'clientid': clientid}
            self.newDataReceived.emit(data)
            self.log.debug('Update position of object {0}'.format(uuid))
        except KeyError:
            self.log.exception('Could not find necessary fields in message')
        except:
            self.log.error('Unknown error occured when updating position')

    def unregister(self, msg):
        try:
            new_uuid = msg['DATA']['TRACKID']
            self.unregisterItem.emit(new_uuid)
            self.log.info('Request unregistering of object with ID {0}'.format(new_uuid))
        except KeyError:
            self.log.warn('Could not find required fields in message', exc_info=True)
            return
