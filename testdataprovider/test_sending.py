import socket
import time
import ast
from jsocket_base import *
import logging

import random
import gpxpy
import gpxpy.gpx

import os

input_file = r'.\qgis_input.txt'
dicts = []
timestamp = 1519290719.



guid = 1

register_dicts = {}
unregister_dicts = {}

def readGpxFile(gpxFileName):
	
	global guid
	global timestamp
	gpx_file = open(gpxFileName, 'r')
	gpx = gpxpy.parse(gpx_file)
	
	dict = []

	for track in gpx.tracks:
		uid = createRegisterEntry(file, dict)
		for segment in track.segments:
			for point in segment.points:
				header = {'TYPE': 'position'}
				data = {'type': 'Feature',
				   'geometry': {'type': 'Point', 'coordinates': [point.longitude, point.latitude]},
				   'properties': {'TRACKID': str(uid),'TIMESTAMP_UNIX': timestamp}}
				tracking_viewer_dict = {'HEADER': header, 'DATA': data}
				timestamp += 1
				dict.append(tracking_viewer_dict)

	unregister_dict2 = {'HEADER': {'TYPE': 'unregister'},
					'DATA':
					{'TRACKID': str(uid),
					'TIMESTAMP_UNIX': timestamp}
					}
	dict.append(unregister_dict2)
	return dict
	
def readNmea(fileName):
	global guid
	global timestamp
	
	dict = []
	with open(fileName) as f:
		uid = createRegisterEntry(fileName, dict)
		for line in f:
			current_dict = ast.literal_eval(line)
			if not ('lat' in current_dict and 'lon' in current_dict):
				continue
			header = {'TYPE': 'position'}
			data = {'type': 'Feature',
					'geometry': {'type': 'Point', 'coordinates': [current_dict['lon'], current_dict['lat']]},
					'properties': {'TRACKID': str(uid),
						'TIMESTAMP_UNIX': timestamp}}

			tracking_viewer_dict = {'HEADER': header, 'DATA': data}
			timestamp += 1
			dict.append(tracking_viewer_dict)
			
	unregister_dict = {'HEADER': {'TYPE': 'unregister'},
                'DATA':
                {'TRACKID': str(uid),
                'TIMESTAMP_UNIX': timestamp}
                }	
	dict.append(unregister_dict)
	return dict
	


def createRegisterEntry(clientid = None, dict = None):
	
	global guid

	if (clientid == None):
		clientid = guid

	register_dict = {'HEADER': {'TYPE': 'register'},
					'DATA':
					{'TRACKID': str(guid),
					'CLIENTID': str(clientid),
					'TIMESTAMP_UNIX': timestamp}
					}
	dict.append(register_dict)
#	register_dicts[guid] = register_dict

	#unregister_dict = {'HEADER': {'TYPE': 'unregister'},
 #               'DATA':
 #               {'TRACKID': str(guid),
 #               'TIMESTAMP_UNIX': timestamp}
 #               }	
	#unregister_dicts[guid] = unregister_dict			
	guid +=1
	return guid - 1
	
#	register_dict = {'HEADER': {'TYPE': 'register'},
#					'DATA':
#					{'TRACKID': 'bd65600d866949038a14af88203add38',
#					'CLIENTID': 'Test Train',
#					'TIMESTAMP_UNIX': timestamp}
#					}
					
					
#dicts.append(register_dict)
#with open(input_file) as f:
#	uid = createRegisterEntry(input_file)
#	for line in f:
#		current_dict = ast.literal_eval(line)
#		if not ('lat' in current_dict and 'lon' in current_dict):
#			continue
#		header = {'TYPE': 'position'}
#		data = {'type': 'Feature',
#				'geometry': {'type': 'Point', 'coordinates': [current_dict['lon'], current_dict['lat']]},
#				'properties': {'TRACKID': str(uid),
#					'TIMESTAMP_UNIX': timestamp}}
#
#		tracking_viewer_dict = {'HEADER': header, 'DATA': data}
#		timestamp += 1
#		dicts.append(tracking_viewer_dict)

d = readNmea(input_file)
dicts.append(d)

for file in os.listdir("."):
    if file.endswith(".gpx"):
		d = readGpxFile(file)
		dicts.append(d)



logging.warning('Set up dictionaries')



#k= range(len(dicts))
#random.shuffle(k)

#logging.warning(k)

s = JsonServer(address='127.0.0.1', port=1025)
s.accept_connection()

logging.warning('register train')

idx = 0

while (True):
	n = random.randint(0, len(dicts)-1)
	
	if (len(dicts[n]) > 0):
		value = dicts[n].pop(0)
		s.send_obj(value)
		print(value)
		time.sleep(.1)
	
	if (len(dicts[n]) == 0):
		value = dicts.pop(n)
		print("removed ###################")
		print(value)
		
	if (len(dicts) == 0):
		print("all's sent that ends well")
		break

	idx = idx + 1
	
#	if (idx > 50):
#		break

#for key, value in register_dicts.iteritems():
#    s.send_obj(value)
#    print(value)
#    time.sleep(.1)
#	
##raw_input("Press Enter to continue...")	
#
#for ii in k:
#    s.send_obj(dicts[ii])
#    print(ii)
#    time.sleep(.1)
#
#for key, value in unregister_dicts.iteritems():
#    s.send_obj(value)
#    print(value)
#    time.sleep(.1)


s.close()
