# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TrackingViewer
                                 A QGIS plugin
 Plugin for showing objects
                             -------------------
        begin                : 2018-02-16
        copyright            : (C) 2018 by Christoph Wiesmeyr/AIT Austrian Institute of Technology GmbH
        email                : christoph.wiesmeyr@ait.ac.at
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load TrackingViewer class from file TrackingViewer.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .tracking_viewer import TrackingViewer
    return TrackingViewer(iface)
