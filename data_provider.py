# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TrackingViewer
                                 A QGIS plugin
 Plugin for showing objects
                              -------------------
        begin                : 2018-02-16
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Christoph Wiesmeyr/AIT Austrian Institute of Technology GmbH
        email                : christoph.wiesmeyr@ait.ac.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.Qt import QThread
from PyQt4.QtCore import pyqtSignal
import time
import ast
import logging
from os import path

# from data_parser.parser_gps import GpsParser

from mobile_items import MobileItems


def resolve(name, basepath=None):
    """
    Function that adds the qgis plugin installation directory to the given name parameter.
    E.g. 'test_data.txt' as input will return '~/.qgis2/python/plugins/plugin_dir/test_data.txt'
    :param name: Filename to resolve
    :param basepath: alternative basepath, default: path of current file
    :return: filename of given file in basepath
    """
    if not basepath:
      basepath = path.dirname(path.realpath(__file__))
    return path.join(basepath, name)


class DataProvider(QThread):

    newDataReceived = pyqtSignal(dict)
    registerItem = pyqtSignal(str, str)
    unregisterItem = pyqtSignal(str)
    unregisterAllItems = pyqtSignal()

    def __init__(self, iface, parent=None):

        super(DataProvider, self).__init__(parent)
        self.name = 'Standard'
        self.iface = iface
        self.dicts = []
        self.reversed_dicts = []
        input_file = resolve('qgis_input.txt')
        self.read_file(input_file)
        self.count = 0
        self.mobile_items = MobileItems(self.iface)
        self.mobile_items.subscribePositionProvider(self)

        # logging
        self.log = logging.getLogger('DataProvider')
        self.log.debug('Set up logger')


        self.reversed_dicts = self.reversed_dicts[::-1]

    def read_file(self, f_name):
        with open(f_name, 'r') as f:
            for line in f:
                curr_dict = ast.literal_eval(line)
                curr_dict['uuid'] = 'uuid'
                self.dicts.append(curr_dict)
                curr_dict_rev = ast.literal_eval(line)
                curr_dict_rev['uuid'] = 'uuid2'
                self.reversed_dicts.append(curr_dict_rev)

    def provide_data(self):

        line = self.dicts[self.count]
        self.newDataReceived.emit(line)
        line = self.reversed_dicts[self.count]
        self.newDataReceived.emit(line)
        self.count += 1

    def run(self):

        self.log.debug('Starting data sending thread')


        self.registerItem.emit('uuid', 'Zug 1')
        for ii in range(600):
            if ii == 100:
                self.registerItem.emit('uuid2', 'Zug 2')
            if ii == 200:
                self.unregisterItem.emit('uuid')
                self.log.debug('unregistered uuid')
            self.log.debug('Sending')
            self.provide_data()
            time.sleep(.05)
            self.log.debug('Sent')

#        self.unregisterItem('uuid2')
        self.unregisterItem.emit('uuid2')
        self.log.debug('unregistered uuid2')


    def clean_up(self):

        self.unregisterAllItems.emit()

