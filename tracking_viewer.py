# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TrackingViewer
                                 A QGIS plugin
 Plugin for showing objects
                              -------------------
        begin                : 2018-02-16
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Christoph Wiesmeyr/AIT Austrian Institute of Technology GmbH
        email                : christoph.wiesmeyr@ait.ac.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from tracking_viewer_dialog import TrackingViewerDialog
import os.path
import logging

from position_marker import PositionMarker
from mobile_item import MobileItem
from data_provider import DataProvider
from jsocket_provider import JSocketProvider
# from data_provider2 import DataProvider2

home_dir = os.path.expanduser('~')
log_fname = os.path.join(home_dir, 'TrackingViewer.log')
log_format_str = '%(asctime)-15s %(name)-15s %(message)s'

logging.basicConfig(filename=log_fname, level=logging.INFO, format=log_format_str)
logging.debug('Set up logger')

class TrackingViewer:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'TrackingViewer_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)


        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&TrackingViewer')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'TrackingViewer')
        self.toolbar.setObjectName(u'TrackingViewer')

        # instantiate connection details
        self.ip_address = None
        self.port = None

        # setting provider to None
        self.provider = None

        # logging
        self.log = logging.getLogger()


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('TrackingViewer', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = TrackingViewerDialog()

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/TrackingViewer'
        self.add_action(
            os.path.join(icon_path, 'settings.png'),
            text=self.tr(u'Configure TrackingViewer'),
            callback=self.configure,
            parent=self.iface.mainWindow())
        self.add_action(
            os.path.join(icon_path, 'start.png'),
            text=self.tr(u'Start TrackingViewer'),
            callback=self.start_tracker,
            parent=self.iface.mainWindow())
        self.add_action(
            os.path.join(icon_path, 'start.png'),
            text=self.tr(u'Terminate TrackingViewer'),
            callback=self.terminate,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&TrackingViewer'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def configure(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            self.ip_address = self.dlg.line_edit_ip.text()
            self.port = self.dlg.port_spin_box.value()
            self.log.debug('Set IP Address to: {0}'.format(self.ip_address))
            self.log.debug('Set port to: {0:0.0f}'.format(self.port))

    def start_tracker(self):
        import os
        print(os.getcwd())
        """ Start the tracking with given configuration"""

        self.mobile_items = {}
        if not (self.ip_address is None or self.port is None):
             self.provider = JSocketProvider(self.ip_address, self.port, self.iface)
             self.provider.start()
        else:
             self.log.warn('IP Adress or port not specified')


        # self.mobile_item = MobileItem(self.iface, 'uuid')
        # data = {'lat': 48.12296896666667, 'lon': 14.915827316666666, 'uuid': 'uuid', 'name': u'Test_data_provider', 'time': 1518685269.839}
        # params = {'trackLength': 3}
        # self.mobile_item.processNewData(data)

#        self.provider = DataProvider(self.iface)
        # self.mobile_item.subscribePositionProvider(self.data_provider)
#        self.provider.start()

        # self.provider = DataProvider2()
        # self.mobile_items['uuid'] = MobileItem(self.iface, 'uuid')
        # self.mobile_items['uuid'].subscribePositionProvider(self.provider)
        # self.mobile_items['uuid2'] = MobileItem(self.iface, 'uuid2')
        # self.mobile_items['uuid2'].subscribePositionProvider(self.provider)
        # self.provider.start()

    def terminate(self):
        """ stop tracking """

        self.provider.clean_up()
        

        for item_name in self.mobile_items.keys():

            curr_item = self.mobile_items.pop(item_name)
            curr_item.removeFromCanvas()
