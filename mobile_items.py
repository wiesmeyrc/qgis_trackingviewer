# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MobileItem
                              -------------------
        begin                : 2015-06-05
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Jens Renken/Marum/University of Bremen,
                               (C) 2018 by Christoph Wiesmeyr/AIT Austrian Institute of Technology GmbH
        email                : christoph.wiesmeyr@ait.ac.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import logging
import random

from PyQt4.QtCore import QObject, pyqtSlot, QTimer, pyqtSignal, QMutex
from qgis.core import QgsPoint, QgsCoordinateTransform, \
        QgsCoordinateReferenceSystem, QgsCsException
from qgis.gui import QgsMessageBar
from position_marker import PositionMarker
from PyQt4.QtGui import QLabel, QMovie


class MobileItems(QObject):
    '''
    A Mobile Item that reveives its position from a dataprovider
    and is displayed on the canvas
    Could be everything liek vehicles or simple beacons
    '''

    newPosition = pyqtSignal(float, QgsPoint, float, float)
    newAttitude = pyqtSignal(float, float, float)   # heading, pitch, roll
    timeout = pyqtSignal()

    def __init__(self, iface, params={}, parent=None):
        '''
        Constructor
        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        :param params: A dictionary defining all the properties of the item
        :type params: dictionary
        :param parent: Parent object for the new item. Defaults None.
        :type parent: QObject
        '''
        super(MobileItems, self).__init__(parent)

        self.iface = iface
        self.canvas = iface.mapCanvas()
        self.name = 'MobileItems Manager'
        self.clrs = ['white', 'black', 'red', 'darkRed', 
              'green', 'darkGreen', 'blue', 'darkBlue', 
              'cyan', 'darkCyan', 'magenta', 'darkMagenta', 
              'yellow', 'darkYellow', 'gray', 'darkGray', 'lightGray' ]
        self.clrsidx = 0
        random.shuffle(self.clrs)  #shuffle list to prevent red next to redDark et al.

        self.markers = {}
        self.params = params
        self.n_markers = 0
        self.dataProvider = {}
        self.messageFilter = dict()
        self.coordinates = {}
        self.position = None
        self.lastFix = 0.0
        self.crsXform = QgsCoordinateTransform()
        self.crsXform.setSourceCrs(QgsCoordinateReferenceSystem(4326))
        self.onCrsChange()
        self.canvas.destinationCrsChanged.connect(self.onCrsChange)
        if hasattr(self.canvas, 'magnificationChanged'):
            self.canvas.magnificationChanged.connect(self.onMagnificationChanged)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.timeout)
        self.notifyCount = int(params.get('nofixNotify', 0))
        if self.notifyCount:
            self.timer.timeout.connect(self.notifyTimeout)
        self.timeoutCount = 0
        self.timeoutTime = int(params.get('timeout', 3000))
        self.notifyDuration = int(params.get('NotifyDuration', 0))
        self.enabled = True
        # self.mutex = QMutex()

        self.log = logging.getLogger('MobileItems')

    @pyqtSlot(str, str)
    def registerMarker(self, uuid, name=None):
        
        if uuid in self.markers:
            self.log.warn('Marker with uuid >{0}< already registered'.format(uuid))
            return

        self.n_markers += 1
        
        if name is None:
            name = 'MobileItem_'+str(self.n_markers)

        params = self.params.copy()
        params['Name'] = name
        params['fillColor'] = self.clrs[self.clrsidx % len(self.clrs)]
#        self.log.warn("params[fillColor]: " + params['fillColor'])
        self.clrsidx = self.clrsidx + 1
        
        marker = PositionMarker(self.canvas, params)
        marker.setToolTip(name)

        self.markers[uuid] = marker
        self.coordinates[uuid] = None

    @pyqtSlot(str)
    def removeFromCanvas(self, uuid):
        '''
        Remove the item and its track from the canvas
        '''

        try:
            marker = self.markers.pop(uuid)
        except KeyError:
            self.log.warn('No marker with uuid {0} registered'.format(uuid))
            return
        marker.removeFromCanvas()
        self.log.info('Removed marker with uuid {0} from canvas'.format(uuid))

    @pyqtSlot()
    def removeAllFromCanvas(self):

        for uuid in self.markers.keys():
            self.removeFromCanvas(uuid)

    def subscribePositionProvider(self, provider, filterId=None):
        '''
        Subscribe the provider for this item
        by connecting to the providers signals
        :param provider: Provider to connect to
        :type provider: DataProvider
        :param filterId: Filter Id for this item
        :type filterId:
        '''
        provider.newDataReceived.connect(self.processNewData)
        provider.registerItem.connect(self.registerMarker)
        provider.unregisterItem.connect(self.removeFromCanvas)
        provider.unregisterAllItems.connect(self.removeAllFromCanvas)

    def _getMarker(self, uuid):
        try:
            current_marker = self.markers[uuid]
        except KeyError:
            self.log.warn('Received data for uuid {0} but no marker registered'.format(uuid))
            return None

        return current_marker

    @pyqtSlot(dict)
    def processNewData(self, data):
        '''
        Process incoming data from the data provider
        :param data: Positon or attitude data
        :type data: dict
        '''

        # self.mutex.lock()

        if not self.enabled:
            return

        try:
            uuid = data['uuid']
        except KeyError:
            self.log.warn('Received data cannot be parsed, field "uuid" not present')
            return

        try:
            client_id = data['clientid']
        except KeyError:
            client_id = None

        try:
            current_marker = self.markers[uuid]
        except KeyError:
            self.log.warn('Received data for uuid {0} but no marker registered, registering object'.format(uuid))
            self.registerMarker(uuid, client_id)
            current_marker = self.markers[uuid]

        self.log.debug('Received data for marker with uuid >{0}<'.format(uuid))
        if 'lat' in data and 'lon' in data:
            self.log.debug('Processing new data in mobile item with uuid {0}'.format(uuid))
            self.position = QgsPoint(data['lon'], data['lat'])
            try:
                self.coordinates[uuid] = self.crsXform.transform(self.position)
                current_marker.setMapPosition(self.coordinates[uuid])
                if 'time' in data:
                    self.lastFix = data['time']
                    self.timer.start(self.timeoutTime)
                    self.timeoutCount = 0
            except QgsCsException:
                pass
        else:
            self.log.debug('Could not find necessary fields in dataset >{0}<'.format(data))

        if 'heading' in data:
            current_marker.newHeading(data['heading'])

        # self.mutex.unlock()

    @pyqtSlot(float)
    def onScaleChange(self):
        '''
        Slot called when the map is zoomed
        :param scale: New scale
        :type scale: float
        '''
        for marker in self.markers.values():
            marker.updatePosition()

    @pyqtSlot()
    def onCrsChange(self):
        '''
        SLot called when the mapcanvas CRS is changed
        '''
        crsDst = self.canvas.mapSettings().destinationCrs()
        self.crsXform.setDestCRS(crsDst)
        for marker in self.markers.values():
            marker.updatePosition()

    @pyqtSlot(float)
    def onMagnificationChanged(self, ):
        '''
        Slot called when the map magnification has changed
        :param scale: New scale
        :type scale: float
        '''
        for marker in self.markers.values():
            marker.updateMapMagnification()

    @pyqtSlot()
    def centerOnMap(self, uuid):
        '''
        Center the item on the map
        TODO: Test
        '''
        if self.coordinates[uuid] is not None:
            self.canvas.setCenter(self.coordinates[uuid])
            self.canvas.refresh()

    @pyqtSlot()
    def notifyTimeout(self):
        self.timeoutCount += 1
        if self.timeoutCount == self.notifyCount:
            msg = self.tr(u'No fix for %s since more than %d seconds!') % (self.name, self.timeoutTime * self.timeoutCount / 1000)
            w = self.iface.messageBar().createMessage(self.tr(u'PosiView Attention'), msg)
            l = QLabel(w)
            m = QMovie(':/plugins/PosiView/hand.gif')
            m.setSpeed(75)
            l.setMovie(m)
            m.setParent(l)
            m.start()
            w.layout().addWidget(l)
            self.iface.messageBar().pushWidget(w, QgsMessageBar.CRITICAL, duration=self.notifyDuration)

    # def getTrack(self, uuid):
    #
    #     marker = self._getMarker(uuid)
    #
    #     if marker is None:
    #         return None
    #
    #     tr = [e[1] for e in marker.track]
    #     return tr
    #
    # def applyTrack(self, track, uuid):
    #     marker = self._getMarker(uuid)
    #
    #     if marker is None:
    #         return None
    #
    #     marker.setTrack(track)
