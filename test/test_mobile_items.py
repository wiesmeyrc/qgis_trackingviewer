import unittest
import uuid
from utilities import get_qgis_app

from mobile_items import MobileItems

[qgis_app, canvas, iface, parent] = get_qgis_app()

class MobileItemsClassTest(unittest.TestCase):

    def setUp(self):
        """Test setUp method."""
        # [qgis_app, canvas, iface, parent] = get_qgis_app()
        self.qgis_app = qgis_app
        self.canvas = canvas
        self.iface = iface
        self.parent = parent


    def tearDown(self):
        """Test tearDown method."""
        # self.qgis_app = None
        # self.canvas = None
        # self.iface = None
        # self.parent = None
        pass

    # def test_class_constructor(self):
    #     mobile_items = MobileItems(self.iface)
    #
    #     assert mobile_items.n_markers == 0
    #     assert mobile_items.markers == {}

    def test_marker_registration(self):
        mobile_items = MobileItems(self.iface)
        new_uuid = uuid.uuid4()
        mobile_items.registerMarker(str(new_uuid))

        assert str(new_uuid) in mobile_items.markers
        print(mobile_items.markers[str(new_uuid)].label)

