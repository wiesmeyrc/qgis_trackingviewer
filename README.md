# TrackingViewer

This QGIS Plugin provides basic functionality for visualizing the positions of objects in real time. The positions
of the tracked objects are passed to the plugin via a TCP/IP connection and are encoded in the JSON format.

## Getting started

**_t.b.d_**

## License

```
    TrackingViewer allows to track moving objects in real time.

    Copyright (C) 2018 - AIT Austrian Institute of Technology GmbH

    TrackingViewer Plugin is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with PosiView Plugin.  If not, see <http://www.gnu.org/licenses/>.
```

## Credits

This project was established adapting codes from other open source projects. Namely, there has been input from
the following projects:

* PosiView QGIS plugin ([GitHub](https://github.com/jrenken/qgis-PosiView))
* jsocket Python package ([PyPi package](https://pypi.python.org/pypi/jsocket/1.5))
* Icons: Icon made by Freepik from [www.flaticon.com]()
